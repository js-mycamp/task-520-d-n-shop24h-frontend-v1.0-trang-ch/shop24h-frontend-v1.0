import axios from "axios";
import React, { useEffect, useState } from "react";

const axiosLibraryCallAPI = async (config) => {
  let response = await axios(config);
  return response.data;
};

function LatestProducts() {
  const [apiResponse, setAPIResponse] = useState("");

  const getAllHandler = () => {
    let config = {
      method: "get",
      maxBodyLength: Infinity,
      url: "http://localhost:8000/products",
      headers: {},
    };
    axiosLibraryCallAPI(config)
      .then((response) => {
        setAPIResponse(response);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  useEffect(() => {
    getAllHandler();
  }, []);

  console.log(apiResponse); // In ra response từ API
  console.log(apiResponse.Data)

  return (
    <>
      <div className="col">
        <button className="btn btn-secondary" onClick={getAllHandler}>
          get all
        </button>
      </div>
    </>
  );
}

export default LatestProducts;
