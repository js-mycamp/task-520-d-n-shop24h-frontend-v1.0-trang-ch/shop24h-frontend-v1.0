
import Carousel from 'react-bootstrap/Carousel';





function CarouselContent() {
    return (

        <div className="container-fluid  mt-20 " style={{ "paddingTop": "50px" }}>
            <div className="container">
                <div className="row">
                    <div className="col-sm-12">
                        {/* thêm */}
                        <Carousel fade>
                            {/* 1 */}
                            <Carousel.Item>
                                <div className="row">
                                    <div className="col-sm-6 d-flex align-items-center">
                                        <div>
                                            <h1 >Iphone 14</h1>
                                            <p >iPhone 14 - Đột phá vượt trội trong công nghệ di động! Sự kết hợp hoàn hảo giữa thiết kế tinh tế và hiệu năng mạnh mẽ.
                                                Màn hình Super Retina XDR 6.1 inch sắc nét,
                                                hiển thị màu sắc sống động. </p>
                                            <div className="form-group">
                                                <button className="btn btn-warning mr-3 pl-4 pr-4" >Shop Now  </button>

                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6">
                                        <img src="https://pngimg.com/d/iphone_14_PNG48.png" style={{ "width": "650px", "height": "550px" }} alt=""></img>
                                    </div>
                                </div>
                                <Carousel.Caption>

                                </Carousel.Caption>
                            </Carousel.Item>
                            {/* 2 */}
                            <Carousel.Item>
                                <div className="row">
                                    <div className="col-sm-6 d-flex align-items-center">
                                        <div>
                                            <h1 >ASUS ROG Phone 7 Ultimate </h1>
                                            <p >ASUS ROG (Republic of Gamers). Được ra mắt bởi hãng công nghệ ASUS,
                                                ROG Phone 7 Ultimate hướng đến cộng đồng game thủ và người dùng yêu thích công nghệ mạnh mẽ và hiệu suất cao. </p>
                                            <div className="form-group">
                                                <button className="btn btn-warning mr-3 pl-4 pr-4" >Shop Now  </button>

                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6">
                                        <img src="https://dlcdnwebimgs.asus.com/files/media/C4512AF4-88A1-4B0E-91F7-BE550046216F/v1/features/images/large/1x/animation/kv/phone_right.png" style={{ "width": "650px", "height": "550px" }} alt=""></img>
                                    </div>
                                </div>
                                <Carousel.Caption>

                                </Carousel.Caption>
                            </Carousel.Item>
                            {/* 3 */}
                            <Carousel.Item>
                                <div className="row">
                                    <div className="col-sm-6 d-flex align-items-center">
                                        <div>
                                            <h1 >Samsung Galaxy S23 Ultra</h1>
                                            <p >Samsung Galaxy S23 Ultra là một trong những sản phẩm cao cấp nhất trong dòng điện thoại Samsung Galaxy S Series.
                                                Được ra mắt bởi hãng công nghệ Samsung, Galaxy S23 Ultra được thiết kế và trang bị những tính năng vượt trội nhằm đáp ứng nhu cầu của người dùng yêu thích công nghệ và đòi hỏi hiệu suất tốt nhất.
                                            </p>
                                            <div className="form-group">
                                                <button className="btn btn-warning mr-3 pl-4 pr-4" >Shop Now  </button>

                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6">
                                        <img src="https://static.vecteezy.com/system/resources/previews/022/722/945/original/samsung-galaxy-s23-ultra-transparent-image-free-png.png" style={{ "width": "650px", "height": "550px" }} alt=""></img>
                                    </div>
                                </div>
                                <Carousel.Caption>

                                </Carousel.Caption>
                            </Carousel.Item>
                        </Carousel>
                        {/* thêm */}

                    </div>
                </div>

            </div>
        </div>
        // thêm
    )


}
export default CarouselContent