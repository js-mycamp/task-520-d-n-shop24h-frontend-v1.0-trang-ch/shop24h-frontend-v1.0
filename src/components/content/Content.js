import CarouselContent from "./Carousel"
import LastestProducts from "./LastestProducts"


function Content () {
    
    return(
        <div className="container">
        <CarouselContent/>
        <LastestProducts/>
        </div>
    )
}
export default Content