import {  } from "@mui/material"

import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import AddShoppingCartIcon from '@mui/icons-material/AddShoppingCart';
import NotificationsIcon from '@mui/icons-material/Notifications';
function IconNavBar() {
  return (
    <div className="mt-3">
      <NotificationsIcon sx={{ cursor: "pointer", marginRight: "20px" }}></NotificationsIcon>
      <AccountCircleIcon sx={{ cursor: "pointer", marginRight: "20px" }}></AccountCircleIcon>
      <AddShoppingCartIcon sx={{ cursor: "pointer", marginRight: "20px" }}></AddShoppingCartIcon>

    </div>
  )
}
export default IconNavBar 