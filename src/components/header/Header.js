import IconNavBar from "./IconNavBar"
import Logo from "./Logo"
import Seach from "./Seach"

function Header() {
  return (
    <nav className="row fixed-top" style={{"backgroundColor":"red", "paddingBottom":"20px"}}>
      <div className="col-3">
        <Logo />      </div>
      <div className="col-7">
        <Seach />
      </div>
      <div className="col-2">
        <IconNavBar />
      </div>


    </nav>
  )
}
export default Header